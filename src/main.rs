use std::{
    collections::HashSet,
    fs
};

fn main() {
    let lines = fs::read_to_string("input.txt").expect("something went wrong");

    let mut total_priority = 0;
    let lines: Vec<&str> = lines.lines().collect();

    for chunk in lines.chunks(3) {
            let line_1 = &chunk[0];
            let line_2 = &chunk[1];
            let line_3 = &chunk[2];

            let mut set1: HashSet<char> = line_1.chars().collect();
            let set2: HashSet<char> = line_2.chars().collect();
            let set3: HashSet<char> = line_3.chars().collect();

            set1.retain(|k| set2.contains(k) && set3.contains(k));

            if let Some(&common_item) = set1.iter().next() {
                total_priority += calculate_priority(common_item);
            }
    }
    println!("The sum of the priorities is: {}", total_priority);
}

fn calculate_priority(item: char) -> i32 {
    if item.is_lowercase() {
        (item as i32) - ('a' as i32) + 1
    } else {
        (item as i32) - ('A' as i32) + 27
    }
}
